package platformer.controller;

import platformer.model.Character;
import platformer.model.Score;
import platformer.model.StaminaBar;
import platformer.view.Platform;

import java.util.ArrayList;

public class GameLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 5;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void update(ArrayList<Character> characterList) {

        for (Character character : characterList ) {
            if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
                character.getImageView().tick();
            }

            if (platform.getKeys().isPressed(character.getLeftKey())) {
                if(platform.getKeys().isPressed(character.getDashKey())) {
                    character.dash();
                    character.warpsound();
                }
                character.setScaleX(-1);
                character.moveLeft();
                character.trace();
            }

            if (platform.getKeys().isPressed(character.getRightKey())) {
                if(platform.getKeys().isPressed(character.getDashKey())) {
                    character.dash();
                    character.warpsound();
                }
                character.setScaleX(1);
                character.moveRight();
                character.trace();
            }

            if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey()) ) {
                character.stop();
            }

            if (platform.getKeys().isPressed(character.getUpKey())) {
                character.jumpsound();
                character.jump();
            }
        }
    }

    private void updateScore(ArrayList<Score> scoreList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i=0 ; i<scoreList.size() ; i++) {
                scoreList.get(i).setPoint(characterList.get(i).getScore());
            }
        });
    }
    private void updateBall(ArrayList<Score> scoreList, ArrayList<Character> characterList) {
        javafx.application.Platform.runLater(() -> {
            for (int i=0 ; i<scoreList.size() ; i++) {
                scoreList.get(i).setPoint(characterList.get(i).getBall());
            }
        });
    }
    
    private void updateStamina(ArrayList<Character> characterList,ArrayList<StaminaBar> staminaList) {    	
    	for (int i=0 ; i<staminaList.size() ; i++) {
    		if(characterList.get(i).getStamina()<=125) {
    			staminaList.get(i).setStamina(characterList.get(i).getStamina());
    			characterList.get(i).setStamina(characterList.get(i).getStamina()+5);
    		}
        }
    }

    private void updateMusic(Music ms, ArrayList<Character> characterList){
        for (Character character : characterList ) {
            if (platform.getKeys().isPressed(character.getMuteKey())) {
                character.mute();
                ms.mutebg();

            }
            if (platform.getKeys().isPressed(character.getUnmuteKey())) {
                character.unmute();
                ms.unmutebg();

            }
        }
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            update(platform.getCharacterList());
            updateScore(platform.getScoreList(),platform.getCharacterList());
            updateBall(platform.getBall(),platform.getCharacterList());
            updateStamina(platform.getCharacterList(),platform.getstaminaBarList());
            updateMusic(platform.getMusic(),platform.getCharacterList());
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
