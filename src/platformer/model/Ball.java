package platformer.model;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import platformer.view.Platform;


public class Ball extends Pane {
    private int x;
    private int y;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;

    private Image ball;
    public static final int BALL_WIDTH = 48;
    public static final int BALL_HEIGHT = 48;
    int yVelocity = 0;
    int yAcceleration = 1;
    int yMaxVelocity = (int) Math.ceil(Math.random() * 5);
    private ImageView imageView;
    private int random = (int)Math.floor(Math.random()*Platform.WIDTH);
    private boolean moving = false;

    public Ball(int x, int y,int offsetX ,int offsetY) {
        this.x = x;

        this.y = y;
        this.startX = x;
        this.startY = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.ball = new Image(getClass().getResourceAsStream("/platformer/assets/Pokeball.png"));
        this.imageView = new ImageView(ball);
        this.imageView.setFitWidth(BALL_WIDTH);
        this.imageView.setFitHeight(BALL_HEIGHT);
        getChildren().addAll(this.imageView);
    }

    public void move() {

        setTranslateY(y);
        yVelocity = yVelocity >= yMaxVelocity ? yMaxVelocity : yVelocity + yAcceleration;
        y = y + yVelocity;
        moving = true;
    }
    public void checkReachGameWall() {
        if(y <= 0) {
            y = 0;
        } else if( y+getHeight() >= Platform.HEIGHT) {
            delete();
            repaint();
        }
    }

    public void delete(){
        this.x =0;
        this.y=0;

    }
    public void repaint() {
        move();
    }
    public boolean ismove(){
        return moving;
    }

    public int getX() { return x; }

    public int getY() { return y; }


}
