package platformer.model;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class StaminaBar extends Pane{
	HBox stamBar;
	HBox border;
	double stamina;
	public StaminaBar(int x, int y){
		stamina = 125;
		stamBar = new HBox();
		setTranslateX(x);
	    setTranslateY(y);
	    stamBar.setStyle("-fx-background-color:linear-gradient(to bottom,#00ff00,#00b300);");
	    stamBar.setPrefWidth(stamina);
	    stamBar.getChildren().add(new Label());
	    border = new HBox();
	    border.setStyle("-fx-border-color: black;");
	    border.getChildren().add(stamBar);
	    border.setPrefWidth(125);
	    getChildren().add(border);
	}
	public void setStamina(double stamina) {
		if(stamina<40) {
			stamBar.setStyle("-fx-background-color:linear-gradient(to bottom,#ff3333,#b30000)");
		}else if(stamina<80) {
			stamBar.setStyle("-fx-background-color:linear-gradient(to bottom,#ffff00,#cccc00)");
		}else if(stamina<120){
			 stamBar.setStyle("-fx-background-color:linear-gradient(to bottom,#0066ff,#0047b3);");
		}else {
			 stamBar.setStyle("-fx-background-color:linear-gradient(to bottom,#00ff00,#00b300);");
		}
		this.stamBar.setMaxWidth(stamina);
	}
	
}
